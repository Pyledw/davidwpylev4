<html>
<head>
	<title><?php echo $title ?></title>
        <meta name="robots" content="noindex">
        <meta name="description" content=" ">
        <!--Javascript Reference to jQuery-->
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        
       
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
  
        <?php include_once 'system/helpers/html_helper.php';
        
        
        
        echo '<link rel="shortcut icon" href="'.site_url('css/favicon.ico').'" /><!--Link to Favicon-->';
        
        date_default_timezone_set('America/Chicago');
        
        if(preg_match('/(?i)msie [1-9]/',$_SERVER['HTTP_USER_AGENT'])) 
            {
                echo link_tag('css/IEstyle.css'); 
            }
        else
            {
                echo link_tag('css/MainStyle.css'); 
            }
        
            $url = $_SERVER['REQUEST_URI'];
            $array = split("/", $url);
            
            
        
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).select();
                $("#rightArea").css("display", "none");
                $("#rightArea").show("slide",{direction: 'right'},500);
                $("#leftaArea").css("display", "none");
                $("#leftArea").show("slide",{direction: 'left'},500);
                $("a").click(function(event){   
                    event.preventDefault();
                    linkLocation = this.href;
                    $("#leftArea").hide("slide", {direction: 'left'},500);
                    $("#rightArea").hide("slide", {direction: 'right'},500,redirectPage);
                    
                    //$("#wrapper").fadeOut(1000, redirectPage);      
                });
                
                
                function redirectPage() {
                    window.location = linkLocation;
                    }
                        });
        </script>
</head>
<body>
    <div id="banner">
        <h1>David W Pyle</h1>
        
    </div>
    <div id ="leftArea">
        <img style="padding-top:5px;" src="<?php echo site_url('images/logo.png'); ?>" alt="dwp Logo"/>
        <ul class ="navigationList">
            <li>
                <a 
                    <?php
                        if(in_array("home", $array) || $url == '/')
                            {
                                echo 'class="currentPage"';
                            }
                    ?> 
                href="/home">Home</a>
            </li>
            <li>
                <a
                    <?php
                        if(in_array("about", $array))
                            {
                                echo 'class="currentPage"';
                            }
                    ?> 
                    href="/about">About</a>
            </li>
            <li>
                <a 
                   <?php
                        if(in_array("resume", $array))
                            {
                                echo 'class="currentPage"';
                            }
                    ?>  
                    href="/resume">Resume</a>
            </li>
            <li>
                <a 
                    <?php
                        if(in_array("portfolio", $array))
                            {
                                echo 'class="currentPage"';
                            }
                    ?> 
                    href="/portfolio">Portfolio</a>
            </li>
        </ul>
        <p id="social"><a href="http://www.facebook.com/pyledw/"><img height="40px" src="images/facebook.png"/></a>
            <a href="http://www.twitter.com/davidpyle"><img height="40px" src="images/twitter.png"/></a><br/>
            <a href="http://www.linkedin.com/pub/david-pyle/31/491/98a"><img height="40px" src="images/linkedin.jpg"/></a></p>
        <p>Created By<br/>David W Pyle</p>
    </div>
    <div id="rightArea">
        
