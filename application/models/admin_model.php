<?php
Class admin_Model extends CI_Model
{
    function getAllContents()
    {
      $this -> db -> from('Content');
      $this->db->join('PageContent', 'PageContent.ContentID = Content.ContentID');
      $this->db->join('Pages', 'Pages.ID = PageContent.PageID');
      $query = $this -> db -> get();
      
      
      return $query->result();
      
      
    }
    function getAllPages()
    {
      
      $this->db->from('Pages');
      $query = $this -> db -> get();
      
      
      return $query->result();
      
      
    }
    function getPageContents($pageId = Null)
    {
      $this -> db -> from('Content');
      $this->db->join('PageContent', 'PageContent.ContentID = Content.ContentID');
      $this->db->join('Pages', 'Pages.ID = PageContent.PageID');
      $this->db->where('Pages.Name',$pageId);
      $query = $this -> db -> get();
      
      return $query->result();
    }
    function getContent($contentID = NULL)
    {
        $this -> db -> from('Content');
        $this -> db -> where('ContentID', $contentID);
        
      $query = $this -> db -> get();
      
      
      return $query->result();
      
    }
    
    function updateContent($contentID)
    {
        $data = array(
                'Title' => $this->input->post('Title') ,
                'Content' => $this->input->post('Content'),
            );

        $this -> db -> where('ContentID = ' . "'" . $contentID . "'");
        $this->db->update('Content', $data);
    }
    
    function newContent($pageName)
    {
        $data = array(
                'Title' => $this->input->post('Title') ,
                'Content' => $this->input->post('Content'),
            );
        $this->db->insert('Content', $data);
        $contentID = $this->db->insert_id();
        
        $this -> db -> from('Pages');
        $this -> db -> where('Name',$pageName);
        $query = $this -> db -> get();
        foreach ($query->result() as $value) {
            $pageID = $value->ID;
        }
        
        $data2 = array(
                'PageID' => $pageID ,
                'ContentID' => $contentID,
            );
        $this->db->insert('PageContent', $data2);
        
        
    }
    function DeleteContent($contentID)
    {
        $this->db->delete('Content', array('ContentID' => $contentID)); 
        $this->db->delete('PageContent', array('ContentID' => $contentID)); 
        
    }
}

?>
