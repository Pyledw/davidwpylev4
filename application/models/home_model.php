<?php
Class Home_Model extends CI_Model
{
    function getHomeContents()
    {
      $this -> db -> from('Content');
      $this->db->join('PageContent', 'PageContent.ContentID = Content.ContentID');
      $this->db->join('Pages', 'Pages.ID = PageContent.PageID');
      $this->db->where('Pages.ID = 1');
      $query = $this -> db -> get();
      
      
      return $query->result();
      
      
    }
}

?>
