<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class portfolio extends CI_Controller {

    
        function __construct()
        {
          parent::__construct();
          //This method will have the credentials validation
         
          $this->load->model('portfolio_model');
        }
	function index()
	{
                $data['title'] = "David W Pyle";
                $data['content'] = $this->portfolio_model->getPortfolioContents();
                $this->load->view('templates/header',$data);
		$this->load->view('portfolio/portfolio');
                $this->load->view('templates/footer');
	}
        
}

?>
