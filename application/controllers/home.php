<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

    
        function __construct()
        {
          
          parent::__construct();
          //This method will have the credentials validation
         
          $this->load->model('home_model');
        }
	function index()
	{
                $data['title'] = "David W Pyle";
                $data['content'] = $this->home_model->getHomeContents();
                $this->load->view('templates/header',$data);
		$this->load->view('home/home');
                $this->load->view('templates/footer');
	}
        
}

?>
