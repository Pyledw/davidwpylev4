<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class about extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   
 }
 
 function index()
 {

     $data['title'] = 'About - Corporate-Ads.com';
     
     $this->load->view('templates/header', $data);
     $this->load->view('templates/mainNavBar');
     $this->load->view('about_view');
     $this->load->view('templates/footer');
   
 }

}